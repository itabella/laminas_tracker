<?php

declare(strict_types=1);

namespace User\Controller\Factory;

use Interop\Container\ContainerInterface;
use Laminas\Db\Adapter\Adapter;
use Laminas\ServiceManager\Factory\FactoryInterface;
use User\Controller\ForgotController;
use User\Model\Table\UsersTable;

class ForgotControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ForgotController(
            $container->get(Adapter::class),
            $container->get(UsersTable::class)
        );
    }
}