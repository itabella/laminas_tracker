<?php

declare(strict_types=1);

namespace User\Controller;

use Laminas\Authentication\AuthenticationService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use User\Form\Auth\ForgotForm;
use User\Model\Table\UsersTable;

class ForgotController extends AbstractActionController
{
    private $adapter;
    private $usersTable;

    public function __construct(Adapter $adapter, UsersTable $usersTable)
    {
        $this->adapter = $adapter;
        $this->usersTable = $usersTable;

    }

    public function indexAction()
    {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity()) {
            return $this->redirect()->toRoute('home');
        }

        $forgotForm = new ForgotForm();
        $request = $this->getRequest();
        return (new ViewModel(['form' => $forgotForm]))->setTemplate('user/auth/forgot');
    }
}