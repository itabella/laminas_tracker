<?php

declare(strict_types=1);

namespace User\Controller;

use Laminas\Authentication\Adapter\DbTable\CredentialTreatmentAdapter;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result;
use Laminas\Crypt\Password\Bcrypt;
use Laminas\Db\Adapter\Adapter;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\SessionManager;
use Laminas\View\Model\ViewModel;
use User\Form\Auth\LoginForm;
use User\Model\Table\UsersTable;

class LoginController extends AbstractActionController
{
    private $adapter;
    private $usersTable;

    public function __construct(Adapter $adapter, UsersTable $usersTable)
    {
        $this->adapter = $adapter;
        $this->usersTable = $usersTable;

    }

    public function indexAction()
    {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity()) {
            return $this->redirect()->toRoute('home');
        }

        $loginForm = new LoginForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $formData = $request->getPost()->toArray();
            $loginForm->setInputFilter($this->usersTable->getLoginFormFilter());
            $loginForm->setData($formData);

            if($loginForm->isValid()) {
                $autAdapter = new CredentialTreatmentAdapter($this->adapter);
                $autAdapter->setTableName($this->usersTable->getTable())
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password')
                    ->getDbSelect()->where(['active' => 1]);

                // data from login
                $data = $loginForm->getData();
                $autAdapter->setIdentity($data['email']);

                // password hashing
                $hash = new Bcrypt();

                $info = $this->usersTable->fetcAccountByEmail($data['email']);

                // to compare password from input with table db
                if($hash->verify($data['password'], $info->getPassword())) {
                    $autAdapter->setCredential($info->getPassword());
                } else {
                    $autAdapter->setCredential('');
                }

                $authResult = $auth->authenticate($autAdapter);

                switch ($authResult->getCode()) {
                    case Result::FAILURE_IDENTITY_NOT_FOUND:
                        $this->flashMessager()->addErrorMessage('Unknow Email address');
                        return $this->redirect()->refresh();
                        break;
                    case Result::FAILURE_CREDENTIAL_INVALID:
                        $this->flashMessager()->addErrorMessage('Incorect Password');
                        return $this->redirect()->refresh();
                        break;
                    case Result::SUCCESS:
                        if($data['recall'] == 1) {
                            $session = new SessionManager();
                            $ttl = 18144400; // session live 21 days
                            $session->rememberMe($ttl);
                        }

                        $storage = $auth->getStorage();
                        $storage->write($autAdapter->getResultRowObject(null, ['created', 'modified']));

                        return $this->redirect()->toRoute(
                            'profile',
                            [
                                'id' => $info->getUserId(),
                                'username' => mb_strtolower($info->getUsername())
                            ]
                        );
                        break;
                    default:
                        $this->flashMessager()->addErrorMessage('Auth Failed');
                        return $this->redirect()->refresh();
                        break;
                }

            } else {
                foreach ($loginForm->getMessages() as $messageId => $message) {
                    printf("Validation failure '%s': %s\n", $messageId, $message);
                }
            }
        }
        return (new ViewModel(['form' => $loginForm]))->setTemplate('user/auth/login');
    }
}