<?php

declare(strict_types=1);

namespace User\Controller;

use Laminas\Authentication\AuthenticationService;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use User\Model\Table\UsersTable;

class AdminController extends AbstractActionController
{
    private $userTable;

    public function __construct(UsersTable $usersTable)
    {
        $this->userTable = $usersTable;
    }

    public function indexAction()
    {
        // make sure whoever accesses this page is logged in
        /*$auth = new AuthenticationService();
        if(!$auth->hasIdentity()) {
            return $this->redirect()->toRoute('login');
        }*/

        // make sure that only the admin can acess this page
        /*if(!$this->authPlugin()->getRoleId() == 1) {
            return $this->notFoundAction();
        }*/

        // grab paginator from UsersTable
        $paginator = $this->userTable->fetchAllAccounts(true);
        $page = (int) $this->params()->fromQuery('page', 1);

        $page = ($page < 1) ? 1 : $page;
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(1);

        return new ViewModel(['account' => $paginator]);
    }
}