<?php

declare(strict_types=1);

namespace User\Controller;

use Laminas\Authentication\AuthenticationService;
use RuntimeException;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use User\Form\Auth\CreateForm;
use User\Model\Table\UsersTable;

class AuthController extends AbstractActionController
{
    private $usersTable;

    public function __construct(UsersTable $usersTable)
    {
        $this->usersTable = $usersTable;
    }

    public function createAction()
    {
        # make sure only visitors with no session access this page
        $auth = new AuthenticationService();
        if($auth->hasIdentity()) {
            #if user has session take some else
            return $this->redirect()->toRoute('home');
        }
        $createForm = new CreateForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $formData = $request->getPost()->toArray();

            $createForm->setInputFilter($this->usersTable->getCreateFormFilter());
            $createForm->setData($formData);

            if ($createForm->isValid()) {
                try {
                    $data = $createForm->getData();
                    $this->usersTable->saveAccount($data);
                    $this->flashMessenger()->addSuccessMessage('Account successfully created');
                    return $this->redirect()->toRoute('login');
                } catch (RuntimeException $ex) {
                    $this->flashMessenger()->addErrorMessage($ex->getMessage());
                    return $this->redirect()->refresh();
                }
            } else {
                foreach ($createForm->getMessages() as $messageId => $message) {
                    printf("Validation failure '%s': %s\n", $messageId, $message);
                }
            }
        }
        return new viewModel(['form' => $createForm]);
    }
}