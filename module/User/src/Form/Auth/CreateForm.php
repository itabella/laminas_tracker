<?php
declare(strict_types=1);

namespace User\Form\Auth;

use Laminas\Form\Form;
use Laminas\Form\Element;

class CreateForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct("new_account");

        $this->setAttribute('method', 'post');

        $this->add([
            'type' => Element\Text::class,
            'name' => 'username',
            'options' => [
                'label' => 'Username',
            ],
            'attributes' => [
                'required' => true,
                'size' => 40,
                'maxlength' => 25,
                'pattern' => '^[a-zA-Z0-9]+$',
                'data-toggle' => 'tooltip',
                'class' => 'form-control',
                'title' => 'Username must consist of alphanumeric character only',
                'placeholder' => 'Enter Your Username'
            ]
        ]);

        // select field
        $this->add([
            'type' => Element\Select::class,
            'name' => 'gender',
            'options' => [
                'label' => 'Gender',
                'empty_options' => 'select...',
                'value_options' => [
                    "Female" => "Female",
                    "Male" => "Male",
                    "Other" => "Other",
                ]
            ],
            'attributes' => [
                'required' => true,
                'class' => 'custom-select',
            ]
        ]);

        // email input field
        $this->add([
            'type' => Element\Email::class,
            'name' => 'email',
            'options' => [
                'label' => 'Email Adress',
            ],
            'attributes' => [
                'required' => true,
                'size' => 40,
                'maxlength' => 120,
                'pattern' => '^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$',
                'autocomplete' => false,
                'data-toggle' => 'tooltip',
                'class' => 'form-control',
                'title' => 'Input valid email',
                'placeholde' => 'Enter Your Email'
            ]
        ]);

        // confirm email
        $this->add([
            'type' => Element\Email::class,
            'name' => 'confirm_email',
            'options' => [
                'label' => 'Email Confirm Adress',
            ],
            'attributes' => [
                'required' => true,
                'size' => 40,
                'maxlength' => 120,
                'pattern' => '^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$',
                'autocomplete' => false,
                'data-toggle' => 'tooltip',
                'class' => 'form-control',
                'title' => 'Input valid email',
                'placeholde' => 'Enter Your Confirm Email'
            ]
        ]);

        // birth day select field
        $this->add([
            'type' => Element\DateSelect::class,
            'name' => 'birthday',
            'options' => [
                'label' => 'Select Birthday',
                'create_empty_option' => true,
                'max_year' => date('Y') - 13,
                'year_attributes' => [
                    'class' => 'custom-select w-30'
                ],
                'month_attributes' => [
                    'class' => 'custom-select w-30'
                ],
                'day_attributes' => [
                    'class' => 'custom-select w-30',
                    'id' => 'day'
                ],
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        //password input text field
        $this->add([
            'type' => Element\Password::class,
            'name' => 'password',
            'options' => [
                'label' => 'Password',
            ],
            'attributes' => [
                'required' => true,
                'size' => 40,
                'maxlength' => 25,
                'autocomplere' => false,
                'data-toggle' => 'tooltip',
                'class' => 'form-control',
                'title' => 'password betwen 8 and 25 char',
                'placeholder' => 'Enter your password'
            ]
        ]);

        // confirm password
        $this->add([
            'type' => Element\Password::class,
            'name' => 'confirm_password',
            'options' => [
                'label' => 'Confirm Password',
            ],
            'attributes' => [
                'required' => true,
                'size' => 40,
                'maxlength' => 25,
                'autocomplere' => false,
                'data-toggle' => 'tooltip',
                'class' => 'form-control',
                'title' => 'password betwen 8 and 25 char',
                'placeholder' => 'Enter your password'
            ]
        ]);

        // csrf
        $this->add([
            'type' => Element\Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ]
            ],
        ]);

        // submit buton
        $this->add([
            'type' => Element\Submit::class,
            'name' => 'create_account',
            'attributes' => [
                'value' => 'Create Account',
                'class' => 'btn btn-primary'
            ],
        ]);
    }

}
