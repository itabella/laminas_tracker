<?php

declare(strict_types=1);

namespace User\Form\Auth;

use Laminas\Form\Element;
use Laminas\Form\Form;

class ForgotForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('forgot');
        $this->setAttribute('method', 'post');

        // email addrss input
        $this->add([
            'type' => Element\Email::class,
            'name' => 'email',
            'options' => [
                'label' => 'Email Address',
            ],
            'attributes' => [
                'required' => true,
                'size' => 40,
                'maxlength' => 120,
                'pattern' => '^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$',
                'autocomplete' => false,
                'data-toggle' => 'tooltip',
                'class' => 'form-control',
                'title' => 'Input valid email',
                'placeholde' => 'Enter Your Email'
            ]
        ]);

        // csrf
        $this->add([
            'type' => Element\Csrf::class,
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600,
                ]
            ],
        ]);

        // submit buton
        $this->add([
            'type' => Element\Submit::class,
            'name' => 'forgot',
            'attributes' => [
                'value' => 'Forgot',
                'class' => 'btn btn-primary'
            ],
        ]);
    }
}