<?php

declare(strict_types=1);

namespace User\Model\Entity;

class UserEntity
{
    // user tabel
    protected $user_id;
    protected $username;
    protected $email;
    protected $password;
    protected $birthday;
    protected $gender;
    protected $photo;
    protected $role_id;
    protected $active;
    protected $views;
    protected $created;
    protected $modified;
    // role table
    protected $role;

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        return $this->user_id = $user_id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        return $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        return $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        return $this->password = $password;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setBirthday($birthday)
    {
        return $this->birthday = $birthday;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        return $this->gender = $gender;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo)
    {
        return $this->photo = $photo;
    }

    public function getRoleId()
    {
        return $this->role_id;
    }

    public function setRoleId($role_id)
    {
        return $this->role_id = $role_id;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        return $this->active = $active;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated($created)
    {
        return $this->created = $created;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setModified($modified)
    {
        return $this->modified = $modified;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        return $this->role = $role;
    }
}